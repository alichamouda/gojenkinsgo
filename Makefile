
buildGo:
	go build -o gojenkinsgo

buildDocker:
	docker build -t gojenkinsgo .

run:
	go run .

unitTest:
	mkdir test
	go test -v -cover -coverprofile test/cover.out
	go tool cover -html=test/cover.out -o test/cover.html

blackboxTest:
	echo "test"

dockerfileTest:
	docker run --rm -i hadolint/hadolint < Dockerfile


jenkins-clean:
	rm -rf src/test 
	rm -f  src/report.xml 
	rm -f  src/index.html 

jenkins-prebuild:
	rm ./.golangci.yml
	rm *_test.go

jenkins-unit-test: jenkins-clean
	cd src; \
	mkdir test; \
	go test -v -cover -coverprofile test/cover.out; \
	go tool cover -html=test/cover.out -o index.html

jenkins-static-test: jenkins-clean
	cd src; \
	golangci-lint run

jenkins-hadolint-test:
	hadolint -f json Dockerfile

