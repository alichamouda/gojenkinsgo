package main

import (
	"os"
	"strconv"
)

func convertToInteger(stringNumber string) int {
	convertedNumber, err := strconv.Atoi(stringNumber)
	if err != nil {
		help()
		os.Exit(1)
	}

	return convertedNumber
}
