package main

import (
	"testing"
)

func TestMulti(t *testing.T) {
	var numbers = []string{"2", "3", "3"}

	var result = multiply(numbers)

	if result != 18 {
		t.Fatalf(`Multiply returned bad result. Expected 18, got %d`, result)
	}
}
