package main

import (
	"fmt"
	"os"
)

func resolveMultiCommand(numbers []string) {
	if numbers[0] == "-v" {
		if len(numbers) < MinArgOnVerboseNumber {
			help()
			os.Exit(1)
		}

		fmt.Print("Result = ")
		fmt.Println(multiply(numbers[1:]))
	} else {
		if len(numbers) < MinArgNumber {
			help()
			os.Exit(1)
		}
		fmt.Println(multiply(numbers))
	}
}

func multiply(numbers []string) int {
	var multiplyResult = 1
	for _, element := range numbers {
		multiplyResult *= convertToInteger(element)
	}

	return multiplyResult
}
