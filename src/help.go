package main

import (
	"fmt"
)

func help() {
	fmt.Println("This application is a simple CLI to sum or multiply a list of integers")
	fmt.Println("\tArgs : [ add multi ]")
	fmt.Println("")
	fmt.Println("You can use -v to print out the result in the format Result = X")
	fmt.Println("")
	fmt.Println("Example:")
	fmt.Println("\t$> add -v 5 8 9 6")
	fmt.Println("\t-> Result = 28")
	fmt.Println("")
	fmt.Println("\t$> add 5 8 9 6")
	fmt.Println("\t-> 28")
	fmt.Println("")
	fmt.Println("\t$> multi -v 5 8 9 6")
	fmt.Println("\t-> Result = 2160")
	fmt.Println("")
	fmt.Println("\t$> multi 5 8 9 6")
	fmt.Println("\t-> 2160")
}
