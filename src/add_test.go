package main

import (
	"testing"
)

func TestAdd(t *testing.T) {
	var numbers = []string{"1", "2", "3"}

	var result = add(numbers)

	if result != 6 {
		t.Fatalf(`Add returned bad result. Expected 6, got %d`, result)
	}
}
