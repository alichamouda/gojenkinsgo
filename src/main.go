package main

import (
	"os"
)

const MinArgNumber = 2
const MinArgOnVerboseNumber = 3

func main() {
	if len(os.Args) < MinArgNumber {
		help()
		os.Exit(1)
	}

	switch os.Args[1] {
	case "add":
		resolveAddCommand(os.Args[MinArgNumber:])
	case "multi":
		resolveMultiCommand(os.Args[MinArgNumber:])
	default:
		help()
	}

	os.Exit(0)
}
