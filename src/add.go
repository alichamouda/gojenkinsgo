package main

import (
	"fmt"
	"os"
)

func add(numbers []string) int {
	var sum = 0
	for _, element := range numbers {
		sum += convertToInteger(element)
	}

	return sum
}

func resolveAddCommand(numbers []string) {
	if numbers[0] == "-v" {
		if len(numbers) < MinArgOnVerboseNumber {
			help()
			os.Exit(1)
		}

		fmt.Print("Result = ")
		fmt.Println(add(numbers[1:]))
	} else {
		if len(numbers) < MinArgNumber {
			help()
			os.Exit(1)
		}
		fmt.Println(add(numbers))
	}
}
