FROM golang:alpine3.15 as build
WORKDIR /app
COPY src/ .
RUN rm -rf ./*test.go && \
    rm -rf test && \
    rm -f  src/report.xml && \
    rm -f  src/index.html && \
    rm .golangci.yml && \
    go build -o gojenkinsgo

FROM scratch as final
WORKDIR /app
COPY --from=build /app/gojenkinsgo .
ENV PATH="/app:${PATH}"
CMD ["./gojenkinsgo"]