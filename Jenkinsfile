pipeline {
    agent any
    environment {
        /*
            Ces variables permettent de controler l'état du pipeline
        */

        // Valeur de controle de qualité de code (test statique)
        QUALITY_GATE      = '5'

        // Taux de couverture minimal à avoir
        CODE_COVERAGE     = '15'

        // Repertoire des dockerfiles associés au agents de Jenkins - à ne pas changer
        DOCKER_AGENTS_DIR = 'Dockerfiles'

        // URL du repo Git - à ne pas changer
        GIT_URL           = 'https://gitlab.com/alichamouda/gojenkinsgo.git'

        // Branche git - à ne pas changer
        GIT_BRANCH        = 'main'

        // Nom de l'image finale
        IMAGE_NAME        = 'gojenkinsgo'

        // URL du registre Docker
        DELIVERY_DOCKER_REGISTRY    = 'http://localhost:5000'
        
        // Credentials du registre Docker
        // WITH_DOCKER_CREDENTIALS valeurs attendues = ['true', 'false']
        WITH_DOCKER_CREDENTIALS     = 'false'
        // ID du credentials pour le registre docker - WITH_DOCKER_CREDENTIALS doit être 'true' pour l'activer
        DOCKER_CREDENTIALS          = 'test'

    }
    stages {
        stage('Checkout') {
            agent any
            steps {
                git branch: GIT_BRANCH, changelog: false, poll: false, url: GIT_URL
            }
        }
        stage('Tests') {
            /*  4 Branches de tests sont parallélisées
                    * Dockerfile Testing
                    * Unit & Coverage Testing
                    * Static Testing
                    * Inside Docker Testing
            */
            parallel {
                stage('Dockerfile Testing') {
                    agent {
                        dockerfile {
                            dir DOCKER_AGENTS_DIR
                            filename 'hadolint.Dockerfile'
                        }
                    }

                    steps {
                        // Linteur du Dockerfile de livraison
                        // Le stage est réussi si le linter ne renvoie aucune erreure
                        sh 'make jenkins-hadolint-test'
                    }
                }
                stage('Unit & Coverage Testing') {
                    stages('Unit & Coverage') {
                        stage('Unit test') {
                            agent {
                                dockerfile {
                                    dir DOCKER_AGENTS_DIR
                                    filename 'goenv.Dockerfile'
                                }
                            }
                            steps {
                                // Cette étape permet de tester le code.
                                // La commande génére aussi un rapport dans index.html
                                sh '''
                                make jenkins-unit-test
                                '''
                                publishHTML([
                                    allowMissing: false,
                                    alwaysLinkToLastBuild: false,
                                    keepAll: false, reportDir: 'src',
                                    reportFiles: 'index.html',
                                    reportName: 'HTML Report',
                                    reportTitles: ''])
                            }
                        }
                        stage('Coverage testing') {
                            agent {
                                dockerfile {
                                    dir DOCKER_AGENTS_DIR
                                    filename 'goenv.Dockerfile'
                                }
                            }
                            steps {
                                // Lance la commande de test de couverture
                                sh 'coverage-test.sh'
                            }
                        }
                    }
                }
                stage('Static Testing') {
                    agent {
                        dockerfile {
                            dir DOCKER_AGENTS_DIR
                            filename 'static.Dockerfile'
                        }
                    }

                    steps {
                        // Lance l'outil de test statique
                        sh '''
                            make jenkins-static-test
                        '''
                        // Expose un rapport, et bloque le process si la contrainte n'est pas respectée.
                        recordIssues qualityGates:
                            [[threshold: QUALITY_GATE, type: 'TOTAL', unstable: false]],
                            tools: [checkStyle(pattern: 'src/report.xml')]
                    }
                }
                stage('Inside Docker Testing') {
                    stages('stages') {
                        stage('Preparing Env') {
                            agent {
                                dockerfile {
                                    dir DOCKER_AGENTS_DIR
                                    filename 'test-prepare.Dockerfile'
                                }
                            }
                            steps {
                                // Génère un dockerfile pour le lancer en agent dans l'étape suivante.
                                // insidedocker.Dockerfile
                                sh 'blackbox-test.sh prepare'
                            }
                        }
                        stage('Test') {
                            agent {
                                dockerfile {
                                    dir '.'
                                    // utiliser le Dockerfile généré.
                                    filename 'insidetest.Dockerfile'
                                }
                            }

                            steps {
                                // Tester l'image en black box test.
                                sh 'blackbox-test.sh test'
                            }
                        }
                    }
                }
            }
        }
        stage('Build & Push Docker Image') {
            // Le dernier Stage.
            agent any
            steps {
                script {
                    docker.withRegistry( DELIVERY_DOCKER_REGISTRY,"${WITH_DOCKER_CREDENTIALS == 'true' ? DOCKER_CREDENTIALS : ''}") {
                        docker.build("${IMAGE_NAME}:${env.BUILD_ID}").push("${env.BUILD_NUMBER}")
                        docker.build("${IMAGE_NAME}:${env.BUILD_ID}").push('latest')
                    }
                }
            }
        }
    }
    post {
        always {
            // Nettoyer l'espace de travail
            cleanWs notFailBuild: true
        }
    }
}
