#!/bin/sh

case $1 in
  prepare)
  rm -f insidetest.Dockerfile 
  SCRATCH_LINE=$(cat -n Dockerfile | grep "scratch" | tail -1 |  cut -f 1 | tr -d '\r\n ') 
  BEFORE_SCRATCH_LINE=$(echo "$SCRATCH_LINE - 1" | bc)
  AFTER_SCRATCH_LINE=$(echo "$SCRATCH_LINE + 1" | bc)
  head  -q -n "$BEFORE_SCRATCH_LINE" Dockerfile >> insidetest.Dockerfile
  echo "FROM alpine:3.15.0 as final" >> insidetest.Dockerfile 
  echo "COPY ./Dockerfiles/scripts/ /bin" >> insidetest.Dockerfile 
  echo "RUN chmod -R +x /bin" >> insidetest.Dockerfile 
  tail -q -n +"$AFTER_SCRATCH_LINE" Dockerfile>> insidetest.Dockerfile
  ;;

  test)
    result=$(gojenkinsgo add 5 6)
    [ "$result" = "11" ]
    result=$(gojenkinsgo multi 5 6)
    [ "$result" = "30" ]
    ;;

  *)
    echo "Args required: prepare | test"
    ;;
esac