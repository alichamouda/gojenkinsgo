#!/bin/sh
cd src
res=$(go test -cover| grep coverage | cut -d':' -f2 | sed -e 's/^[[:space:]]*//' | cut -d'%' -f1)
[ 1 -eq "$(echo "${res} > ${CODE_COVERAGE}" | bc)" ]