FROM golang:alpine3.15
WORKDIR /app
COPY ./scripts/. /bin/.
RUN apk add build-base && \
    apk add git && \
    mkdir /app/.cache && \
    chmod 777 /app/.cache && \
    chmod 777 /app && \
    chmod -R +x /bin &&\
    go install github.com/golangci/golangci-lint/cmd/golangci-lint@latest

ENV  GOLANGCI_LINT_CACHE=/app/.cache \
     GOCACHE=/app/.cache