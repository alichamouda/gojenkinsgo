FROM alpine:3.15.0
WORKDIR /app
COPY ./scripts/. /bin/.
RUN chmod -R +x /bin