FROM golang:alpine3.15
WORKDIR /app
ENV GOCACHE=/tmp/.cache \
    GOLANGCI_LINT_CACHE=/tmp/.cache
COPY ./scripts/. /bin/.
RUN apk add build-base && \
    chmod -R +x /bin