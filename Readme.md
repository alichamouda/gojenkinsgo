# GoJenkinsGo

| Maintainer | Repo |
|-           |-     |
|Ali HAMOUDA|https://gitlab.com/alichamouda/gojenkinsgo.git|



## Introduction
Le but de ce projet est de créer une chaîne d'intégration d'une application en utilisant Jenkins. Le process comprend les tests statiques et dynamiques de l'application écrite en GoLang.

## Contenu du projet
```bash

*__
   |__   _readme_assets              # Dossier de ressources pour le README
   |__   Dockerfiles                 # Dossier de fichier docker pour les tests
      |__   scripts                  # Dossier de scripts à executer lors des tests
      |__   *.Dockerfile             # Fichiers pour les jobs de tests de Jenkins

   |
   |__   src                         # Dossier du code source
      |__   .golangci.yml            # Fichier de config du linter de GoLang
      |__   *.go                     # Fichiers GoLang de l'application
      |__   *_test.go                # Fichiers de test unitaire
  
   |   
   |__   .groovylintrc.json          # Fichier de config du linter de Jenkins 
                                     # pour VS Code
   |__   Dockerfile                  # Dockerfile de livraison de l'application
   |__   JenkinsEnv.compose.yml      # Env. pour déployer Jenkins
   |__   JenkinsFile                 # JenkinsFile du projet
   |__   Makefile                    # Commandes de dev / Jenkins
   |__   Readme.md                   # Documentation

```
<br>

## Guide d'installation
Pour tester la chaîne d'intégration, il faut suivre ces étapes:

1. Installer les plugins sur Jenkins
```bash
    Docker Pipeline
    Warning Next Generation
    HTML Publisher
    Workspace Cleanup Plugin
```
2. Redémarrer Jenkins
3. [OPTION] Lancer cette commande dans le *Script Console* de Jenkins
```java
System.setProperty("hudson.model.DirectoryBrowserSupport.CSP",
                   "script-src 'unsafe-inline'");
```
Cette option permet d'avoir les scripts nécessaires pour une meilleure utilisiation du rapport HTML généré par les tests.

4. Créer un pipeline
5. Configurer le pipeline par le contenue du Jenkinsfile
6. Modifier si nécessaire le contenu des variables exposés au début de la config:
``` Groovy
    environment {
        /*
            Ces variables permettent de controler l'état du pipeline
        */

        // Valeur de controle de qualité de code (test statique)
        QUALITY_GATE      = '5'

        // Taux de couverture minimal à avoir
        CODE_COVERAGE     = '15'

        // Repertoire des dockerfiles associés au agents de Jenkins - à ne pas changer
        DOCKER_AGENTS_DIR = 'Dockerfiles'

        // URL du repo Git - à ne pas changer
        GIT_URL           = 'https://gitlab.com/alichamouda/gojenkinsgo.git'

        // Branche git - à ne pas changer
        GIT_BRANCH        = 'main'

        // Nom de l'image finale
        IMAGE_NAME        = 'gojenkinsgo'

        // URL du registre Docker
        DELIVERY_DOCKER_REGISTRY    = 'http://localhost:5000'
        
        // Credentials du registre Docker
        // WITH_DOCKER_CREDENTIALS valeurs attendues = ['true', 'false']
        WITH_DOCKER_CREDENTIALS     = 'false'
        // ID du credentials pour le registre docker - WITH_DOCKER_CREDENTIALS doit être 'true' pour l'activer
        DOCKER_CREDENTIALS          = 'test'

    }
```


## Présentation de l'application
L'application est une simple calculatrice en ligne de commande.
Elle execute l'une des deux fonctions: **addition** ou **multiplication**.
```bash
    $> gojenkinsgo add 5 6
       11
    $> gojenkinsgo add -v 5 6
       Result = 11
    $> gojenkinsgo multi 5 6
       30
    $> gojenkinsgo multi -v 5 6
       Result = 30
    # En utilisant Docker
    $> docker build -t gojenkinsgo .
    $> docker run gojenkinsgo gojenkinsgo add 5 2
       7
```
<br>

## Plugins Jenkins Utilisés
Ces plugins ont été utilisé pour le pipeline.
Ils s'ajoutent aux plugins qui s'installe par défaut dans Jenkins.

* Docker Pipeline

Ce plugin est important pour ne pas faire le ***build*** et le ***push*** des images Docker par commande. Il permet aussi de lancer les agents spécifique à ce projets décrits par des ***Dockerfiles***.


* HTML Publisher plugin

Installé par défaut, il permet de rendre accessible le rapport de test unitaire et couverture de test.

* Warning next generation

Ce plugin permet de suivre la qualité de code en se basant sur les rapports des tests statiques. Il ajoute des graphs de suivies ainsi que des limites à ne pas dépasser de problèmes par le biais des ***Quality Gates***.

* Workspace Cleanup Plugin
J'ai utilisé ce plugin pour nettoyer l'espace de travail comme post-script.
<br>

## Pipeline

<br>

![Pipeline](_readme_assets/pipeline.png)

### Processus
#### 1. Checkout
Durant cette étape, on récupère le code à partir du repo Git.
#### 2.1 Tests - Dockerfile Testing
En utilisant Hadolint, on test la qualité du Dockerfile de livraison.
Le ***stage*** se base sur l'agent du Dockerfile **Dockerfiles/hadolint.Dockerfile**

#### 2.2. Tests - Inside Docker Testing
* Preparation de l'env

Cette étape génère un dockerfile **insidedocker.dockerfile** à partir  du dockerfile de livraison.
Le fichier **Dockerfile** livre une image à base de ***SCRATCH***: cette image n'a pas ni les commandes de tests, ni un shell **sh**.
La génération d'un docker file est donc importante pour tester l'application après dockerisation.
Ce *stage* se base sur l'agent Dockerfile **Dockerfiles/test-prepare.Dockerfile**
* Test

Durant ce stage, le script de test execute des commandes et compare le résultat par le résultat attendu.

L'agent de ce stage est le fichier généré dans l'étape précédente.

#### 2.3. Tests - Static Testing
Ce stage se base sur l'agent du *Dockerfile* **Dockerfiles/static.Dockerfile**.
Il lance la commande **golangci-lint** qui est un linteur de GoLang (écrit en GoLang).
Cette commande interprète le fichier de config du linter **src/.golangci.yml** pour générer un rapport **checkstyle**.
Ensuite, ce rapport sera interprété par le plugin **Warning next generation**.
Une valeur de **Quality Gate** est mise pour configurer la tolérance au erreurs de styles

#### 2.4. Tests - Unit & Coverage
* Unit test

Ce *stage* se base sur l'agent Dockerfile **Dockerfiles/goenv.Dockerfile**.
Il execute les tests unitaire de l'application et génère un rapport HTML.
Ce rapport sera ensuite mis en ligne par le plugin **HTML Publisher plugin**.

* Coverage Test

Ce stage lance la commande coverage-test.sh qui parse la sortie des tests unitaires et comparer le taux de couverture à celui précisé dans les variables d'environnement au début du **Jenkins File**.

#### 3. Build and Push Docker Image

Ce stage permet de livrer l'image de l'application si tout les tests sont réussis.

On trouve une condition pour activer l'utilisation des credentials. Jenkins ne crée pas les variables d'environnement si elle sont vide (comme '').
Donc, j'ai opté pour ce mécanisme pour remedier à ce problème.

### Environnement d'execution en Docker
- [X] goenv.Dockerfile

- [X] hadolint.Dockerfile

- [X] static.Dockerfile

- [X] test-prepare.Dockerfile

- [X] [Généré] insidetest.Dockerfile

- [X] [Livraison] Dockerfile

### Rapports et livrables
* src/rapport.xml

Résultat du test statique en format checkstyle.

* index.html

Résultat des tests unitaires, contient le css et js. Donne une idée par fichier sur la couverture de test.

* Image Docker

L'image à executer, livrée dans un registre d'image.

## Analyse de résultats
J'ai réussi à créer un pipeline qui test et livre mon application. Certes, cet outil est fort en termes de capacité de réalisation de pipeline complexes mais ceci a un coût.
J'ai trouvé pas mal de problèmes ou erreurs sans trouver la bonne documentation. J'ai passé par exemple une longue période comprendre pourquoi une variable vide est mal interprété par Jenkins. J'ai aussi essayé de livrer mon image avec un entrypoint (au lieu de CMD) dans le Dockerfile, cela ne marche pas pour un bug.
Trouver un outil assez puissant mais moins compliqué sera bien pour la communauté DevOps.


## Conclusion
Ce projet m'a fait appris les différents types de tests et outils de tests. J'ai aussi compris l'importance de suivre la qualité de livrable au cours du temps.
J'ai aussi appris à construire un pipeline en utilisant Jenkins. Je le trouve un outil puissant.




